public class TestAccount{
    
    public static void main(String[] args) {
        Account myAccount = new Account();

        myAccount.setName("Emmanuel");
        myAccount.setBalance(1000000);


        System.out.println("My account name is: " + myAccount.getName());
        System.out.println("My account Balance is: " + myAccount.getBalance());

        myAccount.addInterest();
        System.out.println("My account Balance is:" + myAccount.getBalance());

    }

}