public class CurrentAccount extends Account {

    public CurrentAccount(double balance, String name ){

        super.setBalance(balance);
        super.setName(name);

    }
    @Override
    public void addInterest() {
	    setBalance(getBalance() *  1.1);
    }
    
}