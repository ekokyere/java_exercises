Package com.conygre.simple
public class TestAccount2 {
    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[5];
        Account.setInterestRate(0.5);


        String[] nameArray = {"John","Jack","Emmanuel","James","Peter"};
        double[] balanceArray = {1,10,100,1000,10000};

        for(int i = 0; i <5;i++){
            arrayOfAccounts[i] = new Account(nameArray[i],balanceArray[i]);

            System.out.println("Name of the account:" +i +" is: " + arrayOfAccounts[i].getName() +
                               ". The account balance is: "+ arrayOfAccounts[i].getBalance());

            //add interest
            arrayOfAccounts[i].addInterest();

            System.out.println("The new account balance after interest is:" + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].withdraw();
            
        }

    
    
    }
}