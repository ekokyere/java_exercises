public class Account 
{
    private String name;
    private double balance;
    private static double interestRate;



    public Account(String name,double balance){
        this.name = name;
        this.balance = balance;

    }

    public Account(){
        name = "Emmanuel";
        balance = 50;
    }

    public void withdraw(){
      withdraw(20);

    }

    public boolean withdraw(int amount_to_withdraw){
        if(balance >= amount_to_withdraw){
            System.out.println("Can withdraw");
            return true;
        }else {
            System.out.println("Can't withdraw");
            return false;
            
        }

    }
	public String getName() {
		return name;
	}

	public void setName(String name) {
        this.name = name;
    }
    
	public double getBalance(){
        return balance;
    }
    
    public void setBalance(double balance){
        this.balance = balance;
    }

    public static void setInterestRate( double newInterestRate){
        interestRate = newInterestRate;
    }
    public  static double getInterestRate(){

         return interestRate;
    }
    public void addInterest(){
    }

    
} 